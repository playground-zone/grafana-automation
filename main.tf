terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/30614907/terraform/state/az-state"
    lock_address ="https://gitlab.com/api/v4/projects/30614907/terraform/state/az-state/lock"
    unlock_address ="https://gitlab.com/api/v4/projects/30614907/terraform/state/az-state/lock"     
    lock_method ="POST"
    unlock_method ="DELETE"
    retry_wait_min = "5"
    }
 required_providers {
    grafana = {
      source = "grafana/grafana"
      version = "1.14.0"
    }
  }
}
 provider "grafana" {
  url = var.GRAFANA_URL
  auth = var.GRAFANA_AUTH_TOKEN
  }
resource "grafana_folder" "collection" {
  title = "Demo"
}

resource "grafana_dashboard" "Demo-Board" {
  folder      = grafana_folder.collection.id
  config_json = file("grafana-dashboard.json")
}



